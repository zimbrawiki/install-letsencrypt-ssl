#!/bin/bash

## Stop Zimbra Proxy Service

sudo su - zimbra -c "zmproxyctl stop"
sudo su - zimbra -c "zmmailboxdctl stop"

## Generate First SSL Certificate

export ZIMBRAHOSTNAME=$(hostname -f)
export EMAIL=info@yourdomain.com
sudo certbot certonly --standalone -d $ZIMBRAHOSTNAME -m $EMAIL --agree-tos -n --keep-until-expiring

## Copy Certificate files.

sudo cat /etc/letsencrypt/live/$ZIMBRAHOSTNAME/cert.pem | sudo tee /opt/zimbra/ssl/letsencrypt/cert.pem
sudo cat /etc/letsencrypt/live/$ZIMBRAHOSTNAME/chain.pem | sudo tee /opt/zimbra/ssl/letsencrypt/chain.pem
sudo cat /etc/letsencrypt/live/$ZIMBRAHOSTNAME/fullchain.pem | sudo tee /opt/zimbra/ssl/letsencrypt/fullchain.pem
sudo cat /etc/letsencrypt/live/$ZIMBRAHOSTNAME/privkey.pem | sudo tee /opt/zimbra/ssl/letsencrypt/privkey.pem

## Download IdenTrust root Certificate 

wget https://letsencrypt.org/certs/trustid-x3-root.pem.txt -O /tmp/root.pem 
cat /tmp/root.pem /opt/zimbra/ssl/letsencrypt/chain.pem | sudo tee  /opt/zimbra/ssl/letsencrypt/zimbra_chain.pem

##Set correct permissions for the directory:

sudo chown -R zimbra:zimbra /opt/zimbra/ssl/letsencrypt/

## Copy the private key under Zimbra SSL path.

sudo cat /opt/zimbra/ssl/letsencrypt/privkey.pem | sudo tee /opt/zimbra/ssl/zimbra/commercial/commercial.key
sudo chown zimbra:zimbra /opt/zimbra/ssl/zimbra/commercial/commercial.key

## Deploy the new Let’s Encrypt SSL certificate.

sudo su - zimbra -c '/opt/zimbra/bin/zmcertmgr deploycrt comm  /opt/zimbra/ssl/letsencrypt/cert.pem  /opt/zimbra/ssl/letsencrypt/zimbra_chain.pem'

## Restart the nginx or jetty services stopped earlier.

sudo su - zimbra -c "zmcontrol restart"
