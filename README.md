# Secure Zimbra Server with Let’s Encrypt SSL Certificate

## Introduction

This guide will show you how to easily secure your Zimbra Mail Server with Let’s Encrypt SSL certificate. The default installation of Zimbra generates self-signed SSL certificate for Mails services – POP3/IMAP/SMTP over TLS and for HTTPS access to Zimbra console services.

A self-signed certificate can be used for test deployments but for Production setups I recommend you get a commercial certificate to give your business credibility and better security. If you don’t have a budget for established CA certificate you can use free Let’s Encrypt Certificate to secure your Zimbra server.

Let's Encrypt offers free certificates valid for 3 months, after which they are free to renew. In this guide we are going to deploy Let's Encrypt for Zimbra Community Edition 8.8.15 which is running on Ubuntu 18.04 LTS Operating System. 

You need to create a **`sudo`** user with with **`NOPASSWD: ALL`** option which will require during auto renewal of your certficate.

## Create the `sudo` user

Add the following entry to `/etc/sudoers` configuration file.

```
sudouser   ALL=(ALL:ALL) NOPASSWD:ALL
```

## installating `cerbot` tool

To install Let's Encrypt SSL certificate creation tool **`certbot`** execute the following commands:

```
sudo apt install cerbot
```

## Stop Zimbra Proxy Service

We need to stop the jetty or nginx service services before we can configure it to use Let’s Encrypt SSL certificate.

```
sudo su - zimbra -c "zmproxyctl stop"
sudo su - zimbra -c "zmmailboxdctl stop"
```

## Generate First SSL Certificate

Execute the following command to generate our first SSL certificate.

```
export ZIMBRAHOSTNAME=$(hostname -f)
export EMAIL=info@yourdomain.com
sudo certbot certonly --standalone -d $ZIMBRAHOSTNAME -m $EMAIL --agree-tos -n --keep-until-expiring
```

You can find all your files under **`/etc/letsencrypt/live/$ZIMBRAHOSTNAME`** directory.

## Secure Zimbra Mail Server with Let’s Encrypt SSL Certificate

Create directory that will hold Let’s Encrypt certificates for Zimbra Server.

```
mkdir -p mkdir /opt/zimbra/ssl/letsencrypt
```

## Copy Certificate files.

```
sudo cat /etc/letsencrypt/live/$ZIMBRAHOSTNAME/cert.pem | sudo tee /opt/zimbra/ssl/letsencrypt/cert.pem
sudo cat /etc/letsencrypt/live/$ZIMBRAHOSTNAME/chain.pem | sudo tee /opt/zimbra/ssl/letsencrypt/chain.pem
sudo cat /etc/letsencrypt/live/$ZIMBRAHOSTNAME/fullchain.pem | sudo tee /opt/zimbra/ssl/letsencrypt/fullchain.pem
sudo cat /etc/letsencrypt/live/$ZIMBRAHOSTNAME/privkey.pem | sudo tee /opt/zimbra/ssl/letsencrypt/privkey.pem
```
Confirm files are copied successfully.

```
$ ls /opt/zimbra/ssl/letsencrypt/
```
## Download IdenTrust root Certificate 

We now need to build a proper Intermediate CA plus Root CA. You must to use the IdenTrust root Certificate and merge it after the `chain.pem`. 


```
wget https://letsencrypt.org/certs/trustid-x3-root.pem.txt -O /tmp/root.pem 
cat /tmp/root.pem /opt/zimbra/ssl/letsencrypt/chain.pem | sudo tee  /opt/zimbra/ssl/letsencrypt/zimbra_chain.pem
```
Confirm that `zimbra_chain.pem` created successfully.

```
sudo cat  /opt/zimbra/ssl/letsencrypt/zimbra_chain.pem
```

## Set correct permissions for the directory:

```
sudo chown -R zimbra:zimbra /opt/zimbra/ssl/letsencrypt/
```

## Confirm the owner is zimbra user.

```
sudo ls -lha /opt/zimbra/ssl/letsencrypt/
```

## Verify your commercial certificate.

The command below is one liner. 

```
sudo su - zimbra -c '/opt/zimbra/bin/zmcertmgr verifycrt comm 
/opt/zimbra/ssl/letsencrypt/privkey.pem 
/opt/zimbra/ssl/letsencrypt/cert.pem 
/opt/zimbra/ssl/letsencrypt/zimbra_chain.pem'
```

## Backup current certificate files.

```
sudo cp -a /opt/zimbra/ssl/zimbra /opt/zimbra/ssl/zimbra.$(date "+%Y%.m%.d-%H.%M")
```

## Copy the private key under Zimbra SSL path.

```
sudo cat /opt/zimbra/ssl/letsencrypt/privkey.pem | sudo tee /opt/zimbra/ssl/zimbra/commercial/commercial.key
sudo chown zimbra:zimbra /opt/zimbra/ssl/zimbra/commercial/commercial.key
```

## Deploy the new Let’s Encrypt SSL certificate.

```
sudo su - zimbra -c '/opt/zimbra/bin/zmcertmgr deploycrt comm 
/opt/zimbra/ssl/letsencrypt/cert.pem 
/opt/zimbra/ssl/letsencrypt/zimbra_chain.pem'
```

## Restart the nginx or jetty services stopped earlier.

```
sudo su - zimbra -c "zmcontrol restart"
```

## Test Let’s Encrypt SSL Certificate on Zimbra Mail Server

Open the Admin or webmail console of Zimbra collaboration server and check certificate details.

## Create cron job to run the script every 3 month

We need create a cron job to run it every month, renewing the certificate if it is about to expire. Make sure to change email address `email@yourdomain.com` into your actual values.

## Clone the repository

```
git clone https://gitlab.com/zimbrawiki/install-letsencrypt-ssl
cd install-letsencrypt-ssl
sudo cp le-ssl-cert-auto-renew.sh /usr/local/bin
sudo chmod +x le-ssl-cert-auto-renew.sh
```

Create new cron job that runs on the 1st every month at 2am

```
(crontab -l && echo "0 2 1 * * /bin/sh /usr/local/bin/le-ssl-cert-auto-renew.sh") | crontab -

```
